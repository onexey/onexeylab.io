# onexey.gitlab.io / mesutsoylu.com

[![pipeline status](https://gitlab.com/onexey/onexey.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/onexey/onexey.gitlab.io/-/commits/master)

## What is this

So, I'm in my apartment. Bored from watching Netflix. Thinking; what should I do today.

Decided to try something new and that would be REST API with .NET Core 3. Find myself a [tutorial](https://www.youtube.com/watch?v=sdlt3-ptt9g). But before starting it I wanted to look for git page of the creator if he put the sample code there.

Under repositories section I saw a project called "personal-website". I have also one (and you know that obviously, because you are here). I'm familiar with concept of putting personal site to git but never tried it myself.

A little flashback to yesterday. I got an e-mail from my hosting provider. They notified me; my yearly payment time has come and they prepared everything for me. I just need to go their website and pay the quote and I'm good to go.

So here I am trying to migrate my website from my hosting provider to GitLab Pages.

**What I've learned so far:**

- Jekyll is a great tool for simple websites. Maybe even perfect for hackathons.
- GitLab you are the best. I'm using you for my personal projects and for work. And now I discovered I can put my static website in there without paying any money.
- CI/CD pipelines may not be as hard as I think.
- DO NOT! I repeat DO NOT put your static files to a folder named "public" when using GitLab Pages. They became unreachable. Cost me a few hours to figure this out. Although it wasn't my problem but the most popular suggestion was checking "baseurl" variable in Jekyll _config.yml. For the user pages it should be empty.

**These are the next steps for me:**

- Right now, I'm still trying to learn Jekyll but I think I got the basics.
- There is a little manual labor for the migration. I am okay with it.
  - All migration completed.
- I need to find a theme for my new site.
  - Completed. Used [Hyde Theme](https://github.com/poole/hyde).
- I need to implement some kind of search functionality. Just because my personal site also kind of my notebook.
  - Completed. Used [this](https://github.com/christian-fei/Simple-Jekyll-Search) library.
- I'll redirect my domain to GitLab Pages.
  - Completed this too. Everything points to GitLab Pages and domains have valid SSL certificate.

## In the end

Migrated a very basic WordPress site to Jekyll in two afternoons. This includes learning Jekyll which, basics took about an hour. And I also spend a few hours troubleshooting why my image, css and js files won't show up when published.

To export my post from WP I used a plugin named [Jekyll Exporter](https://wordpress.org/plugins/jekyll-exporter/). Cleaned up exported files a bit because of the way I used WP.

This was a fun weekend project. I'll definitely use GitLab Pages and Jekyll whenever I can.

---
layout: page
permalink: "/credits/"
---
# All resources used in this site

- [Jekyll](https://jekyllrb.com/)
- [Hyde Theme](https://github.com/poole/hyde)
- [Simple Jekyll Search](https://github.com/christian-fei/Simple-Jekyll-Search)
- [Platform icons](https://www.iconfinder.com/iconsets/logos-and-brands)
- [Info icon](https://www.iconfinder.com/iconsets/user-interface-ui-1-set)

---
date: 2020-05-13T17:18:00+03:00
title: "Microsoft Security Compliance Toolkit 1.0"
link: "https://docs.microsoft.com/en-us/windows/security/threat-protection/security-compliance-toolkit-10"
---
[{{ page.title }}]({{ page.link }})

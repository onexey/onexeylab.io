---
layout: page
title: About Me
date: 2017-10-14T22:44:24+03:00
permalink: /about/
---

I am a life long software engineering student. I write bugs, and sometimes fix them. Generally there is also a final product, but that's just a side effect 🙂

At my current workplace ([Stackpole International](http://stackpole.com/)) I mostly work on .Net platform with Microsoft technologies. Handling a few linux servers. Read info-sec stuff, but not as much as I like. A little this, a little that.

Trying to do some hand work when I have time and follow/admire some DIYers. I like psychology, observe and analyze.

I like movies and will watch one as long as I can keep my eyes open. [Here](http://www.imdb.com/user/ur19723454/watchlist) is a list all the movies that I watched.

You can check out my professional profile in [here](https://www.linkedin.com/in/mesutsoylu) and my projects in [here](https://gitlab.com/onexey).

**Mesut Soylu**
